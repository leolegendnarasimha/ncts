use crate::ts::teamspeak::TeamSpeak;
use anyhow::Result;
use colored::*;
use std::{io, io::Write};

pub struct Console {
    teamspeak: TeamSpeak,
}

impl Console {
    pub fn new(teamspeak: TeamSpeak) -> Self {
        Console { teamspeak }
    }

    pub async fn start(&mut self) -> Result<()> {
        loop {
            let mut user_command = String::new();
            print!("{}", "Command: ".blue());
            io::stdout().flush()?;
            //read command from user
            io::stdin()
                .read_line(&mut user_command)
                .expect("Failed to get stdin!");

            //check if command exists
            let mut args: Vec<&str> = user_command.split(" ").collect();
            for (index, arg) in args.clone().into_iter().enumerate() {
                let command: Vec<&str> = arg.trim().split(" ").collect();
                let command = command[0];
                match command {
                    "help" => {
                        println!(
                            "Available commands:\n
                        help\t\tshows this help message\n
                        connect\t\tstarts connection to the TeamSpeak server\n
                        disconnect\tdisconnects from the current TeamSpeak server\n
                        exit\t\texits ncts"
                        )
                    }
                    "connect" => {
                        self.teamspeak.connect().await?;
                    }
                    "disconnect" => {
                        if self.teamspeak.get_handle().lock().unwrap().is_some() {
                            self.teamspeak.disconnect().await?;
                        } else {
                            println!("Not connected to a server");
                        }
                    }
                    "set_subscribed" => {
                        if args.len() > index + 1 {
                            if self.teamspeak.get_handle().lock().unwrap().is_some() {
                                let should_subscribe = args[index + 1];
                                self.teamspeak
                                    .set_subscribed(Some(should_subscribe == "true"))
                                    .await?;
                            } else {
                                println!("Not connected to a server");
                            }
                        }
                        break;
                    }
                    "msg" => {
                        if args.len() > index + 1 {
                            if self.teamspeak.get_handle().lock().unwrap().is_some() {
                                args.remove(index);
                                let message = args.join(" ");
                                self.teamspeak.send_message(message.to_string()).await?;
                            } else {
                                println!("Not connected to a server!")
                            }
                        }
                        break;
                    }
                    "join_channel" | "jc" => {
                        if args.len() > index + 1 {
                            if self.teamspeak.get_handle().lock().unwrap().is_some() {
                                let channel_name = args[index + 1];
                                let channel_password = if args.len() > index + 2 {
                                    args[index + 2]
                                } else {
                                    ""
                                };
                            } else {
                                println!("Not connected to a server!")
                            }
                        }
                        break;
                    }
                    "exit" => {
                        if self.teamspeak.get_handle().lock().unwrap().is_some() {
                            self.teamspeak.disconnect().await?;
                        }
                        std::process::exit(0);
                    }
                    _ => {
                        println!(
                            "{}",
                            format!(
                                "{} \"{}\" {}",
                                "Command".red(),
                                command.trim().blue(),
                                "not found! Run \"help\" to see available commands.".red()
                            )
                        )
                    }
                }
            }
        }
    }
}
