use super::{base, base::BaseView};
use cursive::views::{LinearLayout, ScrollView, TextView};
use cursive_multiplex::Mux;
use std::path::PathBuf;
use tsclientlib::Connection;

pub struct HomeView {
    server_list: Vec<Connection>,
    config_dir: PathBuf,
    key_file: PathBuf,
}

impl HomeView {
    pub fn new(
        server_list: Option<Vec<Connection>>,
        config_dir: PathBuf,
        key_file: PathBuf,
    ) -> Self {
        HomeView {
            server_list: match server_list {
                Some(list) => list,
                _ => {
                    let list: Vec<Connection> = Vec::new();
                    list
                }
            },
            config_dir,
            key_file,
        }
    }

    //Show main home view
    pub fn show(&self) {
        let toolbar = base::get_default_toolbar(self.config_dir.clone(), self.key_file.clone());
        let statusbar = base::get_default_statusbar();
        let mut home_layout = LinearLayout::vertical();
        //TODO: Show server list
        home_layout.add_child(TextView::new("Server list should be here!"));
        let mut mux = Mux::new();
        let node_server_list = mux
            .add_right_of(ScrollView::new(home_layout), mux.root().build().unwrap())
            .expect("Adding view failed!");

        let mut shortcuts: Vec<(char, &str)> = Vec::new();
        shortcuts.push(('q', "QUIT"));
        let base_view = BaseView::new(toolbar, statusbar, mux, shortcuts);
        base_view.show();
    }
}
